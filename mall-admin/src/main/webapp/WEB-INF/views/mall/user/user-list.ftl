<#--
/****************************************************
 * Description: 用户表的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>用户名</th>
	        <th>密码 md5加密存储</th>
	        <th>注册手机号</th>
	        <th>注册邮箱</th>
	        <th>sex</th>
	        <th>address</th>
	        <th>state</th>
	        <th>description</th>
	        <th>role_id</th>
	        <th>头像</th>
	        <th>created</th>
	        <th>updated</th>
	        <th>操作</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
			    ${item.username}
			</td>
			<td>
			    ${item.password}
			</td>
			<td>
			    ${item.phone}
			</td>
			<td>
			    ${item.email}
			</td>
			<td>
			    ${item.sex}
			</td>
			<td>
			    ${item.address}
			</td>
			<td>
			    ${item.state}
			</td>
			<td>
			    ${item.description}
			</td>
			<td>
			    ${item.roleId}
			</td>
			<td>
			    ${item.file}
			</td>
			<td>
			    ${item.created?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
			    ${item.updated?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
            	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/mall/user/input/${item.id}','修改用户表','${tabId}');">修改</@button>
				<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/mall/user/delete/${item.id}','删除用户表？',false,{id:'${tabId}'});">删除</@button>
            </td>
		</tr>
		</#list>
	</tbody>
</@list>