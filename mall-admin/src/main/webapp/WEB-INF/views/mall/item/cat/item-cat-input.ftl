<#--
/****************************************************
 * Description: 商品类目的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/item/cat/save" id=tabId>
   <input type="hidden" name="id" value="${itemCat.id}"/>
   
   <@formgroup title='父分类ID=0时代表一级根分类'>
	<input type="text" name="parentId" value="${itemCat.parentId}" check-type="number">
   </@formgroup>
   <@formgroup title='分类名称'>
	<input type="text" name="name" value="${itemCat.name}" >
   </@formgroup>
   <@formgroup title='状态 1启用 0禁用'>
	<input type="text" name="status" value="${itemCat.status}" check-type="number">
   </@formgroup>
   <@formgroup title='排列序号'>
	<input type="text" name="sortOrder" value="${itemCat.sortOrder}" check-type="number">
   </@formgroup>
   <@formgroup title='是否为父分类 1为true 0为false'>
	<input type="text" name="isParent" value="${itemCat.isParent}" check-type="number">
   </@formgroup>
   <@formgroup title='图标'>
	<input type="text" name="icon" value="${itemCat.icon}" >
   </@formgroup>
   <@formgroup title='备注'>
	<input type="text" name="remark" value="${itemCat.remark}" >
   </@formgroup>
   <@formgroup title='创建时间'>
	<@date name="created" dateValue=itemCat.created  default=true/>
   </@formgroup>
   <@formgroup title='更新时间'>
	<@date name="updated" dateValue=itemCat.updated  default=true/>
   </@formgroup>
</@input>