/****************************************************
 * Description: Entity for 商品描述表
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import java.util.Date;
import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ItemDescEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public ItemDescEntity(){}
    private Long itemId;//商品ID
    private String itemDesc;//商品描述
    private Date created;//创建时间
    private Date updated;//更新时间
    /**
     * 返回商品ID
     * @return 商品ID
     */
    public Long getItemId() {
        return itemId;
    }
    
    /**
     * 设置商品ID
     * @param itemId 商品ID
     */
    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }
    
    /**
     * 返回商品描述
     * @return 商品描述
     */
    public String getItemDesc() {
        return itemDesc;
    }
    
    /**
     * 设置商品描述
     * @param itemDesc 商品描述
     */
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }
    
    /**
     * 返回创建时间
     * @return 创建时间
     */
    public Date getCreated() {
        return created;
    }
    
    /**
     * 设置创建时间
     * @param created 创建时间
     */
    public void setCreated(Date created) {
        this.created = created;
    }
    
    /**
     * 返回更新时间
     * @return 更新时间
     */
    public Date getUpdated() {
        return updated;
    }
    
    /**
     * 设置更新时间
     * @param updated 更新时间
     */
    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.ItemDescEntity").append("ID="+this.getId()).toString();
    }
}

