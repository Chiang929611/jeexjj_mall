/****************************************************
 * Description: Entity for t_mall_base
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class BaseEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public BaseEntity(){}
    private String webName;//web_name
    private String keyWord;//key_word
    private String description;//description
    private String sourcePath;//source_path
    private String uploadPath;//upload_path
    private String copyright;//copyright
    private String countCode;//count_code
    private Integer hasLogNotice;//has_log_notice
    private String logNotice;//log_notice
    private Integer hasAllNotice;//has_all_notice
    private String allNotice;//all_notice
    private String notice;//notice
    private String updateLog;//update_log
    private String frontUrl;//front_url
    /**
     * 返回web_name
     * @return web_name
     */
    public String getWebName() {
        return webName;
    }
    
    /**
     * 设置web_name
     * @param webName web_name
     */
    public void setWebName(String webName) {
        this.webName = webName;
    }
    
    /**
     * 返回key_word
     * @return key_word
     */
    public String getKeyWord() {
        return keyWord;
    }
    
    /**
     * 设置key_word
     * @param keyWord key_word
     */
    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }
    
    /**
     * 返回description
     * @return description
     */
    public String getDescription() {
        return description;
    }
    
    /**
     * 设置description
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * 返回source_path
     * @return source_path
     */
    public String getSourcePath() {
        return sourcePath;
    }
    
    /**
     * 设置source_path
     * @param sourcePath source_path
     */
    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }
    
    /**
     * 返回upload_path
     * @return upload_path
     */
    public String getUploadPath() {
        return uploadPath;
    }
    
    /**
     * 设置upload_path
     * @param uploadPath upload_path
     */
    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }
    
    /**
     * 返回copyright
     * @return copyright
     */
    public String getCopyright() {
        return copyright;
    }
    
    /**
     * 设置copyright
     * @param copyright copyright
     */
    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }
    
    /**
     * 返回count_code
     * @return count_code
     */
    public String getCountCode() {
        return countCode;
    }
    
    /**
     * 设置count_code
     * @param countCode count_code
     */
    public void setCountCode(String countCode) {
        this.countCode = countCode;
    }
    
    /**
     * 返回has_log_notice
     * @return has_log_notice
     */
    public Integer getHasLogNotice() {
        return hasLogNotice;
    }
    
    /**
     * 设置has_log_notice
     * @param hasLogNotice has_log_notice
     */
    public void setHasLogNotice(Integer hasLogNotice) {
        this.hasLogNotice = hasLogNotice;
    }
    
    /**
     * 返回log_notice
     * @return log_notice
     */
    public String getLogNotice() {
        return logNotice;
    }
    
    /**
     * 设置log_notice
     * @param logNotice log_notice
     */
    public void setLogNotice(String logNotice) {
        this.logNotice = logNotice;
    }
    
    /**
     * 返回has_all_notice
     * @return has_all_notice
     */
    public Integer getHasAllNotice() {
        return hasAllNotice;
    }
    
    /**
     * 设置has_all_notice
     * @param hasAllNotice has_all_notice
     */
    public void setHasAllNotice(Integer hasAllNotice) {
        this.hasAllNotice = hasAllNotice;
    }
    
    /**
     * 返回all_notice
     * @return all_notice
     */
    public String getAllNotice() {
        return allNotice;
    }
    
    /**
     * 设置all_notice
     * @param allNotice all_notice
     */
    public void setAllNotice(String allNotice) {
        this.allNotice = allNotice;
    }
    
    /**
     * 返回notice
     * @return notice
     */
    public String getNotice() {
        return notice;
    }
    
    /**
     * 设置notice
     * @param notice notice
     */
    public void setNotice(String notice) {
        this.notice = notice;
    }
    
    /**
     * 返回update_log
     * @return update_log
     */
    public String getUpdateLog() {
        return updateLog;
    }
    
    /**
     * 设置update_log
     * @param updateLog update_log
     */
    public void setUpdateLog(String updateLog) {
        this.updateLog = updateLog;
    }
    
    /**
     * 返回front_url
     * @return front_url
     */
    public String getFrontUrl() {
        return frontUrl;
    }
    
    /**
     * 设置front_url
     * @param frontUrl front_url
     */
    public void setFrontUrl(String frontUrl) {
        this.frontUrl = frontUrl;
    }
    

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.BaseEntity").append("ID="+this.getId()).toString();
    }
}

