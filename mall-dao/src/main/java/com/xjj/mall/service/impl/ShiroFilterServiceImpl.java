/****************************************************
 * Description: ServiceImpl for t_mall_shiro_filter
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.mall.entity.ShiroFilterEntity;
import com.xjj.mall.dao.ShiroFilterDao;
import com.xjj.mall.service.ShiroFilterService;

@Service
public class ShiroFilterServiceImpl extends XjjServiceSupport<ShiroFilterEntity> implements ShiroFilterService {

	@Autowired
	private ShiroFilterDao shiroFilterDao;

	@Override
	public XjjDAO<ShiroFilterEntity> getDao() {
		
		return shiroFilterDao;
	}
}